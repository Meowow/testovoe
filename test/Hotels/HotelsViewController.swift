import UIKit

class HotelsViewController: UIViewController {
    //MARK: - Variables
    private var viewModel: HotelsViewModelProtocol
    private var coordinator: Coordinator
    
    private var hotels = [HotelModel]()
    
    //MARK: - Outlets
    @IBOutlet weak var hotelsTableView: UITableView!
    @IBOutlet weak var loadActivityIndicator: UIActivityIndicatorView!
    
    //MARK: - API
    init(viewModel: HotelsViewModelProtocol, coordinator: Coordinator) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Lifecycle VC
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        hotelsTableView.register(UINib(nibName: "HotelCell", bundle: Bundle.main), forCellReuseIdentifier: "HotelCell")
        viewModel.getHotels()
        setupViewModel()
    }
    
    private func setupViewModel() {
        viewModel.didGetHotels = { [weak self] hotels in
            self?.hotels = hotels
            DispatchQueue.main.async {
                self?.hotelsTableView.reloadData()
                self?.loadActivityIndicator.isHidden = true
            }
        }
    }
    
    //MARK: - User interaction
    private func setupNavigationBar() {
        title = "Отели"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Отсортировать", style: .done, target: self, action: #selector(sortHotelsButtonTapped))
    }
    
    @objc private func sortHotelsButtonTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let sortByDistanceAction = UIAlertAction(title: "по расстоянию", style: .default) { [weak self] _ in
            self?.viewModel.sortHotelsByDistance()
        }
        let sortByFreeRoomsAction = UIAlertAction(title: "по наличию номеров", style: .default) { [weak self] _ in
            self?.viewModel.sortHotelsByRooms()
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        alertController.addAction(sortByDistanceAction)
        alertController.addAction(sortByFreeRoomsAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
}

//MARK: - TableView Delegates & DataSource
extension HotelsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HotelCell", for: indexPath) as! HotelCell
        let hotel = hotels[indexPath.row]
        cell.configure(hotelModel: hotel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let hotel = hotels[indexPath.row]
        coordinator.openDetailHotel(hotelId: hotel.id)
    }
}

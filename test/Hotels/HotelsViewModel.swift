import Foundation

protocol HotelsViewModelProtocol {
    var didGetHotels: (([HotelModel]) -> Void)? { get set }
    func getHotels()
    func sortHotelsByDistance()
    func sortHotelsByRooms()
}

class HotelsViewModel: HotelsViewModelProtocol {
    //MARK: - Outputs
    public var didGetHotels: (([HotelModel]) -> Void)?
    private var hotels = [HotelModel]() {
        didSet {
            self.didGetHotels?(hotels)
        }
    }
    
    //MARK: - Variables
    private var isSortedByDistance = false
    private var isSortedByRooms = false
    private let networkingService: NetworkingService
    
    init(networkingService: NetworkingService) {
        self.networkingService = networkingService
    }
    
    //MARK: - Inputs
    public func getHotels() {
        networkingService.getHotels { [weak self] (hotels) in
            self?.hotels = hotels
        }
    }
    
    public func sortHotelsByDistance() {
        if isSortedByDistance {
            hotels.sort(by: {$0.distance > $1.distance})
        } else {
            hotels.sort(by: {$0.distance < $1.distance})
        }
        isSortedByDistance.toggle()
    }
    
    public func sortHotelsByRooms() {
        if isSortedByRooms {
            hotels.sort(by: {$0.suitesAvailability.convertStringToArray().count < $1.suitesAvailability.convertStringToArray().count})
        } else {
            hotels.sort(by: {$0.suitesAvailability.convertStringToArray().count > $1.suitesAvailability.convertStringToArray().count})
        }
        isSortedByRooms.toggle()
    }
}

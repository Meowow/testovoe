import UIKit

class HotelCell: UITableViewCell {

    @IBOutlet weak var hotelNameLabel: UILabel!
    @IBOutlet weak var distanceToHotelLabel: UILabel!
    @IBOutlet weak var hotelAddressLabel: UILabel!
    @IBOutlet weak var emptyRoomsInHotelLabel: UILabel!
    @IBOutlet weak var hotelStarsLabel: UILabel!
    
    
    public func configure(hotelModel: HotelModel) {
        hotelNameLabel.text = hotelModel.name
        distanceToHotelLabel.text = "\(hotelModel.distance) метров"
        hotelAddressLabel.text = hotelModel.address
        checkEmptyNumbers(rooms: hotelModel.suitesAvailability)
        hotelStarsLabel.text = "\(hotelModel.stars)"
    }
    
    private func checkEmptyNumbers(rooms: String) {
        if rooms.isEmpty {
            emptyRoomsInHotelLabel.text = "Нет свободных номеров"
        } else {
            emptyRoomsInHotelLabel.text = "Свободно номеров: \(rooms.convertStringToArray().count)"
        }
    }
    
}

import Foundation

struct HotelModel: Decodable {
    let id: Int
    let name: String
    let address: String
    let stars: Int
    let image: String?
    let distance: Float
    let suitesAvailability: String

    enum CodingKeys: String, CodingKey {
        case id, name, address, stars, distance, image
        case suitesAvailability = "suites_availability"
    }
}

import Foundation

extension String {
    public func convertStringToArray() -> [String] {
        return self.components(separatedBy: ":")
    }
}

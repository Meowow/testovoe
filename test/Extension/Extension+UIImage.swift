import UIKit

extension UIImage {
    public func cropImage() -> UIImage{
        guard let croppedImage = self.cgImage?.cropping(to: CGRect(x: 1, y: 1, width: self.size.width - 2, height: self.size.height - 2)) else { return UIImage() }
        let image = UIImage(cgImage: croppedImage)
        return image
    }
}

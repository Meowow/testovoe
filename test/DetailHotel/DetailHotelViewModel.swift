import UIKit

protocol DetailHotelViewModelProtocol {
    var didGetHotel: ((HotelModel) -> Void)? { get set }
    var didGetImage: ((UIImage) -> Void)? { get set }
    func getHotel(hotelId: Int)
}

class DetailHotelViewModel: DetailHotelViewModelProtocol {
    public var didGetHotel: ((HotelModel) -> Void)?
    public var didGetImage: ((UIImage) -> Void)?
    private let networkingService: NetworkingService
    
    init(networkingService: NetworkingService) {
        self.networkingService = networkingService
    }
    
    public func getHotel(hotelId: Int) {
        networkingService.getHotel(hotelId: hotelId) { (hotel) in
            self.didGetHotel?(hotel)
            guard let imageId = hotel.image else { return }
            self.getHotelImage(imageId: imageId)
        }
    }
    
    private func getHotelImage(imageId: String) {
        networkingService.downloadImage(imagePath: imageId) { (image) in
            self.didGetImage?(image.cropImage())
            
        }
    }
   
}

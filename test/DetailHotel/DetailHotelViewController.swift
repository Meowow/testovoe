import UIKit

class DetailHotelViewController: UIViewController {

    private var viewModel: DetailHotelViewModelProtocol
    
    @IBOutlet weak var hotelImageView: UIImageView!
    @IBOutlet weak var hotelNameLabel: UILabel!
    @IBOutlet weak var hotelDistanceLabel: UILabel!
    @IBOutlet weak var hotelAddressLabel: UILabel!
    @IBOutlet weak var hotelRoomsLabel: UILabel!
    
    var hotelId: Int?
    
    init(viewModel: DetailHotelViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Подробная информация"
        navigationItem.largeTitleDisplayMode = .never
        viewModel.getHotel(hotelId: hotelId ?? 0)
        setupViewModel()
    }
    
    private func setupViewModel() {
        viewModel.didGetHotel = { [weak self] hotel in
            self?.hotelNameLabel.text = hotel.name
            self?.hotelDistanceLabel.text = "\(hotel.distance) метров"
            self?.hotelAddressLabel.text = hotel.address
            self?.hotelRoomsLabel.text = "Свободные номера: \(hotel.suitesAvailability)"
            
        }
        
        viewModel.didGetImage = { [weak self] image in
            self?.hotelImageView.image = image
        }
    }
    
    



}

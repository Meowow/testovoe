import UIKit

protocol NetworkingService {
    func getHotels(completion: @escaping (([HotelModel])->()))
    func getHotel(hotelId: Int, completion: @escaping ((HotelModel)->()))
    func downloadImage(imagePath: String, completion: @escaping ((UIImage)->()))
}

let imageCache = NSCache<NSString, UIImage>()

class NetworkingApi: NetworkingService {
    private let session = URLSession.shared
    private let urlString = "https://raw.githubusercontent.com/iMofas/ios-android-test/master/"
    public func getHotels(completion: @escaping (([HotelModel]) -> ())) {
        guard let url = URL(string: "\(urlString)0777.json") else { return }
        let request = URLRequest(url: url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil {
                guard let data = data else { return }
                do {
                    let hotels = try JSONDecoder().decode([HotelModel].self, from: data)
                    DispatchQueue.main.async {
                        completion(hotels)
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                
            }
        }
        dataTask.resume()
    }
    
    public func getHotel(hotelId: Int, completion: @escaping ((HotelModel) -> ())) {
        guard let url = URL(string: "\(urlString)\(hotelId).json") else { return }
        let request = URLRequest(url: url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil {
                guard let data = data else { return }
                do {
                    let hotel = try JSONDecoder().decode(HotelModel.self, from: data)
                    DispatchQueue.main.async {
                        completion(hotel)
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                
            }
        }
        dataTask.resume()
    }
    
    func downloadImage(imagePath: String, completion: @escaping ((UIImage)->())) {
        guard let url = URL(string: "\(urlString)\(imagePath)") else { return }
        if let cashedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cashedImage)
        } else {
            let request = URLRequest(url: url)
            let dataTask = session.dataTask(with: request) { (data, response, error) in
                if error == nil {
                    guard let data = data else { return }
                    DispatchQueue.main.async {
                        guard let image = UIImage(data: data) else { return }
                        imageCache.setObject(image, forKey: url.absoluteString as NSString)
                        completion(image)
                    }
                } else {
                    
                }
            }
            dataTask.resume()
        }
    }
    
}

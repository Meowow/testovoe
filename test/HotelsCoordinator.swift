import UIKit

protocol Coordinator {
    var navigationController: UINavigationController { get set }
    func start()
    func openDetailHotel(hotelId: Int)
}

class HotelsCoordinator: Coordinator {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let viewModel = HotelsViewModel(networkingService: NetworkingApi())
        let hotelVC = HotelsViewController(viewModel: viewModel, coordinator: self)
        navigationController.pushViewController(hotelVC, animated: true)
    }
    
    public func openDetailHotel(hotelId: Int) {
        let viewModel = DetailHotelViewModel(networkingService: NetworkingApi())
        let detailHotelVC = DetailHotelViewController(viewModel: viewModel)
        detailHotelVC.hotelId = hotelId
        navigationController.pushViewController(detailHotelVC, animated: true)
    }
    
}
